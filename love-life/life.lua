local cell = require "cell"

local LifeMap = {
  mutateThreshold = 0.1
}
LifeMap.__index = LifeMap

function LifeMap.new(width, height)
  local lifeMap = {}

  setmetatable(lifeMap, LifeMap)
  lifeMap.width = width
  lifeMap.height = height

  lifeMap:regenerate()

  return lifeMap
end

function LifeMap:regenerate()
  for j = 1, self.height do
    self[j] = {}
    for i = 1, self.width do
      local config = {
        alive = math.random() < 0.5,
        colour = math.floor(math.random() * 6) + 1
      }
      self[j][i] = cell.Cell.new(config)
    end
  end
end

function LifeMap:draw()
  for j = 1, self.height do
    for i = 1, self.width do
      self[j][i]:draw((i - 1) * 16, (j - 1) * 16)
    end
  end
end

function LifeMap:lifeCycle()
  for j = 1, self.height do
    for i = 1, self.width do
      local near, colours = self:computeNeighbours(i, j)
      self[j][i]:prepare(near, colours)
    end
  end

  local count = 0
  for j = 1, self.height do
    for i = 1, self.width do
      if self[j][i].alive then
        count = count + 1
      end
      self[j][i]:live()
    end
  end

  if count < self.mutateThreshold then
    self:regenerate()
  end

  local i = math.random()
  while i < self.mutateThreshold do
    self:mutate()
    i = i + 1
  end
end

function LifeMap:computeNeighbours(x, y)
  local near = 0
  local colours = {}

  function collect(x, y)
    near = near + 1
    table.insert(colours, self[y][x].colour)
  end

  if x > 1 and y > 1 and self[y - 1][x - 1].alive then collect(x - 1, y - 1) end
  if x < self.width and y > 1 and self[y - 1][x + 1].alive then collect(x + 1, y - 1) end
  if x > 1 and y < self.height and self[y + 1][x - 1].alive then collect(x - 1, y + 1) end
  if x < self.width and y < self.height and self[y + 1][x + 1].alive then collect(x + 1, y + 1) end
  if x > 1 and self[y][x - 1].alive then collect(x - 1, y) end
  if y > 1 and self[y - 1][x].alive then collect(x, y - 1) end
  if x < self.width and self[y][x + 1].alive then collect(x + 1, y) end
  if y < self.height and self[y + 1][x].alive then collect(x, y + 1) end

  return near, colours
end

function LifeMap:mutate()
  local accum = {}

  for j = 1, self.height do
    for i = 1, self.width do
      if self[j][i].alive then
        table.insert(accum, {i, j})
      end
    end
  end

  if #accum > 0 then
    local dir = math.floor(math.random() * 4)
    local candidate = accum[math.floor(math.random() * #accum) + 1]
    local x, y = candidate[1], candidate[2]

    if dir == 0 and x > 1 then
      x = x - 1
    elseif dir == 1 and y > 1 then
      y = y - 1
    elseif dir == 2 and x < #self[y] then
      x = x + 1
    elseif dir == 3 and y < #self then
      y = y + 1
    end
    self[y][x]:mutate()
  end
end

return {
  LifeMap = LifeMap
}
