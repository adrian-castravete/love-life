local love = require "love"

local Cell = {}
Cell.__index = Cell

function Cell.classSetup()
  Cell.spriteSheet = love.graphics.newImage("sprite-sheet.png")
  Cell.setupAgeQuads()
  Cell.setupEyeQuads()
  Cell.smileQuad = love.graphics.newQuad(58, 12, 4, 2, Cell.spriteSheet:getDimensions())
end

function Cell.setupAgeQuads()
  local ages = {"kid", "adult", "old", "ancient", "dead"}
  local quads = {}
  local width, height = Cell.spriteSheet:getDimensions()

  for j, ageName in ipairs(ages) do
    local age = {}
    for i = 1, 7 do
      table.insert(age, love.graphics.newQuad((j - 1) * 17 + 1, i * 17 + 1, 16, 16, width, height))
    end
    quads[ageName] = age
  end

  Cell.ageQuads = quads
end

function Cell.setupEyeQuads()
  local eyes = {
    center = {55, 2},
    left = {21, 2},
    up = {38, 2},
    right = {21, 10},
    down = {38, 10}
  }
  local quads = {}
  local width, height = Cell.spriteSheet:getDimensions()

  for eyeName, eye in pairs(eyes) do
    quads[eyeName] = love.graphics.newQuad(eye[1], eye[2], 10, 6, width, height)
  end

  Cell.eyeQuads = quads
end

function Cell.new(config)
  local colour = math.floor(math.random() * 6) + 1

  local cell = {
    age = 1,
    eyeDirection = "center",
    alive = false,
    colour = colour,
    changeEyeAge = 1,
    next_age = 1,
    next_alive = false,
    next_colour = colour
  }

  if config then
    for k, v in pairs(cell) do
      local c = config[k]
      if c then
        cell[k] = c
      end
    end
  end

  setmetatable(cell, Cell)

  return cell
end

function Cell:draw(x, y)
  local quad = self:getAgeQuad()
  local equad = self:getEyeQuad()

  love.graphics.draw(self.spriteSheet, quad, x, y)
  if self.alive then
    love.graphics.draw(self.spriteSheet, equad, x + 3, y + 3)
    love.graphics.draw(self.spriteSheet, self.smileQuad, x + 6, y + 10)
  end
end

function Cell:getAgeQuad()
  local age = nil
  local colour = nil

  if self.age <= 10 then
    age = "kid"
  elseif self.age <= 30 then
    age = "adult"
  elseif self.age <= 60 then
    age = "old"
  else
    age = "ancient"
  end
  if self.alive then
    colour = self.colour
  else
    colour = 7
  end

  return self.ageQuads[age][colour]
end

function Cell:getEyeQuad()
  return self.eyeQuads[self.eyeDirection]
end

function Cell:checkEyeChange()
  if self.age > self.changeEyeAge then
    self.changeEyeAge = self.age + math.floor(math.random() * 20)
    local eyeDirection = math.floor(math.random() * 5) + 1
    local dirs = {"center", "left", "up", "right", "down"}

    self.eyeDirection = dirs[eyeDirection]
  end
end

function Cell:prepare(near, colours)
  self.next_alive = self.alive
  self.next_age = self.age + 1
  if not self.alive and near == 3 then
    self.next_colour = self:chooseColour(colours)
    self.next_alive = true
    self.next_age = 1
    self.changeEyeAge = 1
  elseif self.alive and (near < 2 or near > 3) then
    self.next_alive = false
  end
end

function Cell:live()
  self:checkEyeChange()
  self.age = self.next_age
  self.alive = self.next_alive
  self.colour = self.next_colour
end

function Cell:mutate()
  self.next_alive = not self.alive
  self.alive = self.next_alive
  self.next_colour = math.floor(math.random() * 6) + 1
  self.colour = self.next_colour
end

function Cell:chooseColour(colours)
  if #colours == 0 then
    return math.floor(math.random() * 6) + 1
  end

  local candidates = {}

  for i, colour in ipairs(colours) do
    if candidates[colour] then
      candidates[colour] = candidates[colour] + 1
    else
      candidates[colour] = 1
    end
  end

  local top = 0
  for key, value in pairs(candidates) do
    if value > top then
      top = value
    end
  end

  local chooseSet = {}
  for key, value in pairs(candidates) do
    if value == top then
      table.insert(chooseSet, key)
    end
  end

  return chooseSet[math.floor(math.random() * #chooseSet) + 1]
end

Cell.classSetup()

return {
  Cell = Cell
}
