local love = require "love"
local life = require "life"

local displayWidth, displayHeight = 960, 640
local lifeMap = life.LifeMap.new(displayWidth / 16, displayHeight / 16)

function love.load()
  love.window.setTitle("Conway's Game of Life in LÖVE")
  love.window.setMode(displayWidth, displayHeight)
  math.randomseed(love.timer.getTime())
end

function love.draw()
  lifeMap:draw()
end

function love.update(dt)
  lifeMap:lifeCycle()
  love.timer.sleep(0.04)
end

function love.keypressed(key)
  if key == "space" then
    lifeMap:regenerate()
  elseif key == "escape" then
    love.event.quit()
  end
end
