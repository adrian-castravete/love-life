LOVE_NAME_WIN32=love-0.10.1-win32

all:

buildlinux: cleanlove buildlove

buildwindows: cleanlove cleanwindows buildlove
	mkdir -p bin/windows/
	mkdir -p build/

	cd build/ ; \
	if ! [ -e $(LOVE_NAME_WIN32).zip ] ; \
	then \
		wget https://bitbucket.org/rude/love/downloads/$(LOVE_NAME_WIN32).zip ; \
	fi ; \
	if ! [ -e $(LOVE_NAME_WIN32) ] ; \
	then \
		unzip $(LOVE_NAME_WIN32).zip ; \
	fi ;

	cat build/$(LOVE_NAME_WIN32)/love.exe bin/love-life.love > bin/windows/love-life.exe
	cp build/$(LOVE_NAME_WIN32)/*.dll bin/windows/

buildlove:
	mkdir -p bin/
	cd love-life; zip -9 -q -r ../bin/love-life.love .

cleanwindows:
	rm -rf bin/windows/*

cleanlove:
	rm -rf bin/love-life.love
