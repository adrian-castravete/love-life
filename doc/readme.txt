                          ___________________

                               LOVE-LIFE

                           Adrian Castravete
                          ___________________


Table of Contents
_________________

1 Introduction
.. 1.1 Technologies
2 What to do
.. 2.1 Keys
3 Rules
.. 3.1 Staying alive
.. 3.2 Rebirth
4 Particularities
.. 4.1 Race
..... 4.1.1 Flavours:
.. 4.2 Age
.. 4.3 Eyes


[../icons/love-life.png]


1 Introduction
==============

  This project is an attempt at creating a cute implementation of
  *Conway's Game of Life*


1.1 Technologies
~~~~~~~~~~~~~~~~

  The project uses the [LÖVE] game engine and is written in [lua].
  Graphics were created using *GrafX2*.


[LÖVE] http://www.love2d.org/

[lua] https://www.lua.org/


2 What to do
============

  Nothing. Just enjoy the view. :D OK, there are two keys implemented:


2.1 Keys
~~~~~~~~

  *Space* - To regenerate the field. Escape* - To exit the program.


3 Rules
=======

  The rules of the program remain the same standard ones:


3.1 Staying alive
~~~~~~~~~~~~~~~~~

  If a cell is alive and has /2/ or /3/ neighbours, it remains alive.


3.2 Rebirth
~~~~~~~~~~~

  If a cell is already dead and there are /3/ neighbours, it becomes
  alive.


4 Particularities
=================

  There are some differences maybe not present in all implementations.


4.1 Race
~~~~~~~~

  This version has a notion of differences between flavours of cells.
  There are /6/ colours and when a cell becomes alive the predominant
  neighbour colour is used for the newly _born_ cell.


4.1.1 Flavours:
---------------

* 4.1.1.1 red


* 4.1.1.2 yellow


* 4.1.1.3 green


* 4.1.1.4 cyan


* 4.1.1.5 blue


* 4.1.1.6 magenta


4.2 Age
~~~~~~~

  While existent in so many implementations, the age in this version is
  represented by the dimming of the cell. The age is calculated by the
  amount of *frames* that have passed since the _birth_ of the cell.


* 4.2.0.1 kid:

  Anything under 11 frames is considered a kid


* 4.2.0.2 adult:

  Anything from 11 to 30 is considered an adult


* 4.2.0.3 old:

  If between 31 and 60 inclusively, it is considered old


* 4.2.0.4 ancient:

  Anything over 60 is considered ancient


4.3 Eyes
~~~~~~~~

  These are present only for /eye/-candy. No real purpose other than
  being cute.
